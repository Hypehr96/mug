import axios from 'axios'

export function Table(options: any) {
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        Reflect.defineMetadata('apiName', options.api, constructor.prototype);
        Reflect.defineMetadata('entityName', options.entity, constructor.prototype);
        return constructor;
    }
}

export abstract class ModelCrud<T extends ModelCrud<T>> {
    id:number;
    
    async save<T extends ModelCrud<T>>():Promise<T> {
        this.setMetadata();        
        
        if(!this.id)
            return ModelCrud.$create<T>(this);
        else
            return ModelCrud.$update<T>(this.id, this);
    }

    static async $create<T>(object: any):Promise<T> {
        return axios.post(`${this.getBaseUrl()}`, object).then(rsp=>{
            return rsp.data.data;
        }).catch(err=>{
            console.error(err);
            return null;
        });
    };

    static async $read<T>(id?:number, query?:any):Promise<T[]> {        
        return axios.get(`${this.getBaseUrl()}${id?"/"+id:""}${(query?"?"+ModelCrud.serialize(query):"")}`).then(rsp=>{
            return id?[rsp.data.data]:rsp.data.data;
        }).catch(err=>{
            console.error(err);
            return [];
        });
    };

    static async $update<T>(id:number, object: any):Promise<T> {
        return axios.patch(`${this.getBaseUrl()}/${id}`, object).then(rsp=>{
            return rsp.data.data;
        }).catch(err=>{
            console.error(err);
            return null;
        });
    };

    static async $delete<T>(id:number):Promise<any> {
        return axios.delete(`${this.getBaseUrl()}/${id}`).then(rsp=>{
            return rsp.data;
        }).catch(err=>{
            console.error(err);
            return null;
        });
    };
    
    static async findOneBy<T>(options:any):Promise<T>{
        return this.$read<T>(options.id).then(rsp=>{
            return rsp.length>0?rsp[0]:null;
        });
    };

    static async findBy<T>(options:any):Promise<T[]>{
        return this.$read<T>(options.id).then(rsp=>{
            return rsp;
        });
    };

    private setMetadata() {
        const $apiName = Reflect.getMetadata('apiName', this);
        const $entityName = Reflect.getMetadata('entityName', this);
        ModelCrud.setMetadata($apiName, $entityName);
    }

    private static setMetadata(api:string, entity:string) {
        Reflect.defineMetadata('apiName', api, this.prototype);
        Reflect.defineMetadata('entityName', entity, this.prototype);
    }

    private static getBaseUrl():string {
        const $apiName = Reflect.getMetadata('apiName', this.prototype);
        const $entityName = Reflect.getMetadata('entityName', this.prototype);
        
        return `${$apiName}/${$entityName}`;
    }

    private static serialize(obj:object, prefix?:string):string {
        let str = [], p:any;
        for (p in obj) {
          if (obj.hasOwnProperty(p)) {
            let k = prefix ? prefix + "[" + p + "]" : p,
              v = obj[p];
            str.push((v !== null && typeof v === "object") ?
              ModelCrud.serialize(v, k) :
              encodeURIComponent(k) + "=" + encodeURIComponent(v));
          }
        }
        return str.join("&");
    }
}