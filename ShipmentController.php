<?php

namespace App\PanelBundle\Controller;

use App\PanelBundle\Entity\Shipment;
use App\PanelBundle\Form\ShipmentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ShipmentController extends PanelController
{
    public function listAction():Response
    {
       return $this->render('panel/shipment/list.html.twig', []);
    }
    public function formAction(Request $request, EntityManagerInterface $em, int $id = 0):Response
    {
        /** @var Shipment $shipment */
        $shipment = $em
            ->getRepository(Shipment::class)
            ->find($id?:0);

        $edit = !!$shipment->getId();

        $form = $this->createForm(ShipmentType::class, $shipment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($shipment);
            $em->flush();

            if($edit)
                $this->addFlash('success', "Pomyślnie zmieniono sposób dostawy {$shipment->getName()}!");
            else
                $this->addFlash('success', "Pomyślnie dodano sposób dostawy {$shipment->getName()}!");

            return $this->redirectToRoute('panel_shipment');
        }

        return $this->render(
            'panel/shipment/form.html.twig', [
                'form' => $form->createView(),
                'shipment' => $shipment
            ]
        );
    }
    public function removeAction(Request $request, EntityManagerInterface $em, $id):Response
    {
        $out['status']=false;
        /** @var Shipment $shipment */
        $shipment = $em->getRepository(Shipment::class)->find($id);
        if($shipment){
            $em->remove($shipment);
            $em->flush();
            $out['status']=true;
        }

        if($request->isXmlHttpRequest()){
            return new JsonResponse($out);
        } else {
            if($out['status'])
                $this->addFlash('success', "Pomyślnie usunięto sposób dostawy!");
            else
                $this->addFlash('danger', "Nie udało się usunąć sposobu dostawy!");

            return $this->redirectToRoute('panel_shipment');
        }
    }
    public function ajaxAction(Request $request, EntityManagerInterface $em, $param, int $id = null):Response
    {
        $out = [];
        switch($param){
            case 'getList':
                $page = $this->getPage($request, Shipment::class, ["k.id", 'k.name', 'k.price']);

                $columnCount = intval($request->request->get('columnCount'));
                $rows = [];
                $k = 0;

                $shipments = $page->objects;
                foreach($shipments as $shipment) {
                    for ($i = 0; $i < $columnCount; $i++) {
                        $rows[$k][] = $this->renderView('panel/shipment/row.html.twig', [
                            'key' => $i,
                            'shipment' => $shipment
                        ]);
                    }
                    $k++;
                }

                $out = [
                    'aaData' => $rows,
                    'iTotalRecords' => $page->recordsTotal,
                    'iTotalDisplayRecords' => $page->recordsFiltered
                ];
                break;
        }

        return new JsonResponse($out);
    }
}
